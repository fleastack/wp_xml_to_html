#!/usr/bin/php
<?php
# Usage:
#		$ wp_xml_to_html.php wp_export.xml
#
#	By: John Haefele
#	Initial Authoring: 2014-06-11

$wp_xml_file = $argv[1];
if ($wpxml = simplexml_load_file($wp_xml_file)) {
	shell_exec('mkdir wp_export');
	foreach($wpxml->channel->item as $article) {
		$article_filename = strtolower(str_replace(' ','_',$article->title)).'.html';
		$article_title = '<h1>'.$article->title.'</h1>';
		$article_content = $article->children('content', true)->encoded;
		$article_excerpt = $article->children('excerpt', true)->encoded;
		if ($article_content != "" && $article_content != " ") {
			if(!file_put_contents('./wp_export/'.$article_filename, $article_title."\n".($article_excerpt != '' ? "<h4>Excerpt: ".$article_excerpt."</h4>\n" : '').$article_content)) {
				echo 'Failed to create article: '.$article_filename;
			}
		}
	}
	shell_exec('tar -cvzf wp_export_articles.tgz ./wp_export; rm -rf ./wp_export');
}
?>